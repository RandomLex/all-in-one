package copyrows;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 * @author alexej_barzykin@epam.com
 */
public class RowCopier implements Copier {
  @Override
  public int copy(BufferedReader from, BufferedWriter to, int rowCount) throws IOException {
    int count = 0;
    while (from.ready() && count++ < rowCount) {
      to.append(from.readLine()).append('\n');
    }
    return count;
  }
}

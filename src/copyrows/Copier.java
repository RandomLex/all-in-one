package copyrows;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 * @author alexej_barzykin@epam.com
 */
public interface Copier {
  /**
   * Copies from {@link BufferedReader} to {@link BufferedWriter} rowCount lines.
   * If rowCount is less than there are at 'from', all the rows will be copied.
   * Returns the number of copied rows.
   *
   * @param from source as {@link BufferedReader}
   * @param to destination as {@link BufferedWriter}
   * @param rowCount needed amount of copied rows
   * @return actual amount of copied rows
   * @throws IOException if any
   */
  int copy(BufferedReader from, BufferedWriter to, int rowCount) throws IOException;
}

package copyrows;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author alexej_barzykin@epam.com
 */
public class Starter {

  private static final int FROM_FILENAME_INDEX = 0;
  private static final int TO_FILENAME_INDEX = 1;
  private static final int ROWS_TO_COPY_INDEX = 2;

  public static void main(String[] args) {
    int rowsToCopy;
    try {
      rowsToCopy = Integer.parseInt(args[ROWS_TO_COPY_INDEX]);
    } catch (NumberFormatException e) {
      e.printStackTrace();
      throw new IllegalArgumentException(e);
    }
    try (BufferedReader from = new BufferedReader(new FileReader(args[FROM_FILENAME_INDEX]));
         BufferedWriter to = new BufferedWriter(new FileWriter(args[TO_FILENAME_INDEX]))) {
      Copier copier = new RowCopier();
      copier.copy(from, to, rowsToCopy);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}

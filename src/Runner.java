import java.util.stream.IntStream;

/**
 * @author alexej_barzykin@epam.com
 */
public class Runner {
    private static int SOME = IntStream.rangeClosed(0, 100).parallel().reduce(0, Integer::sum);

    public static void main(String[] args) {
        System.out.println(SOME);
    }
}

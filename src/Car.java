import java.io.Serializable;

/**
 * @author alexej_barzykin@epam.com
 */
public class Car implements Cloneable, Serializable {
    private Engine engine;
    private Wheel wheel;

    public Car() {
    }

    public Car(Engine engine, Wheel wheel) {
        this.engine = engine;
        this.wheel = wheel;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public Wheel getWheel() {
        return wheel;
    }

    public void setWheel(Wheel wheel) {
        this.wheel = wheel;
    }

    public Car withEngine(Engine engine) {
        this.setEngine(engine);
        return this;
    }

    public Car withWheel(Wheel wheel) {
        this.setWheel(wheel);
        return this;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "Car{" +
                "engine=" + engine +
                ", wheel=" + wheel +
                '}';
    }
}

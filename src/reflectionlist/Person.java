package reflectionlist;

import java.util.List;

/**
 * @author alexej_barzykin@epam.com
 */
public class Person {
  private int id;
  private String fio;
  private List<Book> books;

  public Person() {
  }

  public Person(int id, String fio, List<Book> books) {
    this.id = id;
    this.fio = fio;
    this.books = books;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getFio() {
    return fio;
  }

  public void setFio(String fio) {
    this.fio = fio;
  }

  public List<Book> getBooks() {
    return books;
  }

  public void setBooks(List<Book> books) {
    this.books = books;
  }


}

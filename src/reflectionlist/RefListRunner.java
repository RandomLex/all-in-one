package reflectionlist;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * @author alexej_barzykin@epam.com
 */
public class RefListRunner {
  public static void main(String[] args) {
    Book alphabet = new Book(1, "Alphabet");
    Book javaForDummies = new Book(2, "Java for Dummies");
    Person alex = new Person(1, "Alexej Barzykin", new ArrayList<>());
    List<Book> books = alex.getBooks();
    books.add(alphabet);
    books.add(javaForDummies);

//    System.out.println(getPersonName(alex));
    List<Book> refBooks = getBooksOfPerson(alex);
    refBooks.forEach(System.out::println);

  }

  private static String getPersonName(Person person) {
    String result = null;
    try {
      Field field = Person.class.getDeclaredField("fio");
      field.setAccessible(true);
      result = (String) field.get(person);
    } catch (NoSuchFieldException | IllegalAccessException e) {
      e.printStackTrace();
    }
    return result;
  }

  private static List<Book> getBooksOfPerson(Person person) {
    try {
      Field field = person.getClass().getDeclaredField("books");
      field.setAccessible(true);
      return (List<Book>)field.get(person);
    } catch (NoSuchFieldException | IllegalAccessException e) {
      e.printStackTrace();
    }
    return new ArrayList<>();
  }
}

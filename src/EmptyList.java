import java.util.ArrayList;
import java.util.List;

/**
 * @author alexej_barzykin@epam.com
 */
public class EmptyList {
    public static void main(String[] args) {
        List<String> strings = new ArrayList<>();
        System.out.println(strings.get(0));
    }
}

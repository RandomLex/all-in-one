package localmaxima;

public class Sample {
    public static void main(String[] args) {
        int[] input = {1, 2, 3};

        System.out.println(sum(input));
    }

    static int sum(int[] ints) {
        if (ints == null || ints.length == 0) {
            return 0;
        }

        int result = 0;
        for (int anInt : ints) {
            result += anInt;
        }
        return result;
    }
}

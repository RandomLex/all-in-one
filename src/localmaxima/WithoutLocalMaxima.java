package localmaxima;

import java.util.Arrays;

public class WithoutLocalMaxima {
    public static void main(String[] args) {
        int[] input = {-18, 21, 3, 6, 7, 65};
//        int[] input = {18, 6, 7, 3, 6, 9, 3, 5, 7, 8};
//        int[] input = {18, 6};
//        int[] input = {6, 18};
//        int[] input = {1, 2, 3, 4, 5};
//        int[] input = {5, 4, 3, 2, 1};
//        int[] input = {1, 1, 1, 1, 1};

        System.out.println(Arrays.toString(input));

        System.out.println(Arrays.toString(withoutLocalMaxima(input)));
    }

    private static int[] withoutLocalMaxima(int[] input) {
        int[] temp = new int[input.length];
        int index = 0;

        if (input[0] <= input[1]) {
            temp[index++] = input[0];
        }

        for (int i = 1; i < input.length - 1; i++) {
            if (input[i] <= input[i-1] || input[i] <= input[i+1]) {
                temp[index++] = input[i];
            }
        }

        if (input[input.length - 1] <= input[input.length - 2]) {
            temp[index++] = input[input.length - 1];
        }

//        int[] result = new int[index];
//        System.arraycopy(temp, 0, result, 0, index);
//        return result;
        return Arrays.copyOf(temp, index);
    }
}

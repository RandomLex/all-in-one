package zinoviev.lambda;

import java.util.Arrays;
import java.util.List;

/**
 * @author alexej_barzykin@epam.com
 */
public class StreamDemo {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("usa", "RUSSIA ", "gerMANY", "JaPaN");

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        for (String s : list) {

        }

        list.forEach(s -> System.out.println(s));

        list.forEach(System.out::println);

        list.stream()
                .forEach(System.out::println);

        for (String item: list) {
            String newItem = item.toLowerCase().trim();
            if (newItem.contains("p")) {
                continue;
            }
            System.out.println(newItem);
        }

        list.stream()
                .map(a -> a.toLowerCase().trim())
                .filter(elem -> !elem.contains("p"))
                .forEach(a -> System.out.println(a));


    }
}

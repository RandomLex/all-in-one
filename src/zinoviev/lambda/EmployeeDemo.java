package zinoviev.lambda;

import zinoviev.lambda.beans.Department;
import zinoviev.lambda.beans.Employee;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author alexej_barzykin@epam.com
 */
public class EmployeeDemo {
    public static void main(String[] args) {
        List<Employee> employees = hireEmployeees();

        employees.stream()
                .filter(e -> e.getLevel() > 2)
                .map(Employee::getDepartment)
                .distinct()
                .sorted(Comparator.comparing(Department::getName))
                .map(Department::getName)
                .forEach(System.out::println);

        System.out.println("Average is "
        + employees.stream()
                .mapToInt(Employee::getAge)
                .summaryStatistics());

        List<Department> deps = new ArrayList<>();

        for (Employee emp: employees) {
            if (emp.getLevel() > 2) {
                deps.add(emp.getDepartment());
            }
        }

        Set<Department> distinctDeps = new HashSet<>(deps);

        List<Department> distinctDepsList = new ArrayList<>(distinctDeps);

        Comparator<Department> comparator = new Comparator<Department>() {
            @Override
            public int compare(Department o1, Department o2) {
                if (o1 == o2) {
                    return 0;
                }
                if (o1 == null) {
                    return -1;
                }
                if (o2 == null) {
                    return 1;
                }
                return o1.getName().compareTo(o2.getName());
            }
        };
        Collections.sort(distinctDepsList, comparator);

        for (Department dep: distinctDepsList) {
            System.out.println(dep.getName());
        }

        int sum = 0;
        int counter = 0;
        for (Employee employee : employees) {
            sum += employee.getAge();
            counter++;
        }

        System.out.println("Avarage is " + (sum/counter));

    }

    private static List<Employee> hireEmployeees() {
        Department financialDepartment = new Department("Finance department");
        Department backendDepartment = new Department("Backend department");

        return Arrays.asList(new Employee(19, "Vasia", "Java", 1, financialDepartment)
                , new Employee(20, "Petja", ".NET", 3, financialDepartment)
                , new Employee(30, "John", "Delphi", 5, backendDepartment)
                , new Employee(45, "Sergey" , "Java", 2, backendDepartment));
    }
}

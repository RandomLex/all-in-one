package zinoviev.lambda.flatmap;

import zinoviev.lambda.beans.Developer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.summingInt;

/**
 * @author alexej_barzykin@epam.com
 */
public class FlatMapDemo {
    public static void main(String[] args) {
        Map<String, Integer> backendSkillMatrix = new HashMap<>();
        backendSkillMatrix.put("Java", 4);
        backendSkillMatrix.put("Scala", 3);
        backendSkillMatrix.put("Kotlin", 1);
        Developer backender = new Developer("Petja", backendSkillMatrix);

        Map<String, Integer> frontendSkillMatrix = new HashMap<>();
        frontendSkillMatrix.put("React", 90);
        frontendSkillMatrix.put("Angular", 80);
        frontendSkillMatrix.put("Kotlin", 4);
        Developer front = new Developer("Vasja", frontendSkillMatrix);

        List<Developer> projectTeam = new ArrayList<>();
        projectTeam.add(backender);
        projectTeam.add(front);

        List<String> result = projectTeam.stream()
                .map(e -> e.getSkillMatrix().entrySet())
                .flatMap(set -> set.stream())
                .peek(System.out::println)
                .filter(item -> item.getValue() > 2)
                .map(item -> item.getKey())
                .distinct()
                .collect(Collectors.toList());

        System.out.println(result);

        Stream<Integer> ints = Stream.of(1, 2, 3, 4, 5, 6, 7);
        System.out.println(ints.mapToInt(i -> i).sum());

        Stream<Integer> ints2 = Stream.of(1, 2, 3, 4, 5, 6, 7);
        System.out.println(ints2.reduce(0, Integer::sum));

        Stream<Integer> ints3 = Stream.of(1, 2, 3, 4, 5, 6, 7);
        System.out.println(ints3.collect(summingInt(Integer::intValue)));

    }
}

package zinoviev.lambda.flatmap;

import zinoviev.lambda.BusinessTaskUpdateCustomerAge;
import zinoviev.lambda.beans.Employee;
import zinoviev.lambda.beans.Department;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;

/**
 * @author alexej_barzykin@epam.com
 */
public class FlatMapDemo2 {
    public static void main(String[] args) {
        List<Employee> employees = BusinessTaskUpdateCustomerAge.hireEmployeees();

        Map<String, Department> result = employees.stream()
                .filter(e -> e.getAge() > 30)
                .sorted(comparing(Employee::getName))
                .collect(Collectors.toMap(Employee::getName, Employee::getDepartment));
        System.out.println(result);
    }
}

package zinoviev.lambda;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * @author alexej_barzykin@epam.com
 */
public class StreamDemo2 {
    public static void main(String[] args) {
        Stream<Integer> stream = Arrays.asList(1, 2, 3, 4, 5, 6).stream()
                .filter(i -> i > 4)
                .map(bbb -> 1);
        System.out.println(stream.count());
    }
}

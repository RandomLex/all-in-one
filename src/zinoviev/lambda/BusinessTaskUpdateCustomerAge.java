package zinoviev.lambda;

import zinoviev.lambda.beans.Department;
import zinoviev.lambda.beans.Employee;

import java.util.Arrays;
import java.util.List;

/**
 * @author alexej_barzykin@epam.com
 */
public class BusinessTaskUpdateCustomerAge implements BusinessTask {

    @Override
    public void updateStateInDB() {
        System.out.println("We are doing that");
    }

    public static List<Employee> hireEmployeees() {
        Department financialDepartment = new Department("Finance department");
        Department backendDepartment = new Department("Backend department");

        return Arrays.asList(new Employee(19, "Vasia", "Java", 1, financialDepartment)
                , new Employee(20, "Petja", ".NET", 3, financialDepartment)
                , new Employee(31, "John", "Delphi", 5, backendDepartment)
                , new Employee(45, "Sergey" , "Java", 2, backendDepartment));
    }
}

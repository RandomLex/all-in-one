package zinoviev.lambda.curried;

import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * @author alexej_barzykin@epam.com
 */
public class CurriedDemo {
    public static void main(String[] args) {
        CalculatePerDiem machine = new CalculatePerDiem();
        Function<Integer, Double> curriedbyFirstArgument = machine.curryFirstArgument(57.16);
        System.out.println(curriedbyFirstArgument.apply(5));
        System.out.println(curriedbyFirstArgument.apply(10));

        Function<Double, Double> curriedBySecondArgument = machine.currySecondArgument(10);
        System.out.println(curriedBySecondArgument.apply(56.12));
        System.out.println(curriedBySecondArgument.apply(30.12));

    }

}

@FunctionalInterface
interface CurriedBiFunction<T, U, R> extends BiFunction<T, U, R> {

    default Function<U, R> curryFirstArgument(T t) {
        return u -> apply(t, u);
    }

    default Function<T, R> currySecondArgument(U u) {
        return t -> apply(t, u);
    }
}

class CalculatePerDiem implements CurriedBiFunction<Double, Integer, Double> {

    private static final Double perDiemRate = 10.15;

    @Override
    public Double apply(Double dollarExchangeRate, Integer amountOfDays) {
        return dollarExchangeRate * amountOfDays * perDiemRate;
    }
}

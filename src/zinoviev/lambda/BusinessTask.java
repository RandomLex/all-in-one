package zinoviev.lambda;

/**
 * @author alexej_barzykin@epam.com
 */
public interface BusinessTask {

    default void updateStateInDBByDefault() {
        System.out.println("I'm not an abstract method in interface.");
    }

    void updateStateInDB();
}

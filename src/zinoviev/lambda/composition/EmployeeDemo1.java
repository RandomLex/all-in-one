package zinoviev.lambda.composition;

import zinoviev.lambda.beans.Department;
import zinoviev.lambda.beans.Employee;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * @author alexej_barzykin@epam.com
 */
public class EmployeeDemo1 {
    public static void main(String[] args) {
        Predicate<Integer> isPrime = number -> isEven(number);

        Consumer<Integer> messageVerification = message -> System.out.println(message);

        Function<String, Department> strangeConstructor = name -> new Department(name);

        Function<Integer, Integer> addTwo = a -> a + 2;
        System.out.println(addTwo.apply(3));

        Supplier<Boolean> random = () -> ThreadLocalRandom.current().nextBoolean();

        Stream.of(1, 2, 3, 4, 5, 6)
                .map(addTwo)
                .filter(isPrime)
                .forEach(messageVerification);


    }

    private static boolean isEven(Integer number) {
        return number % 2 == 0;
    }

    private static List<Employee> hireEmployeees() {
        Department financialDepartment = new Department("Finance department");
        Department backendDepartment = new Department("Backend department");

        return Arrays.asList(new Employee(19, "Vasia", "Java", 1, financialDepartment)
                , new Employee(20, "Petja", ".NET", 3, financialDepartment)
                , new Employee(30, "John", "Delphi", 5, backendDepartment)
                , new Employee(45, "Sergey" , "Java", 2, backendDepartment));
    }
}

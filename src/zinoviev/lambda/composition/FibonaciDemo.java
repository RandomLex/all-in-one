package zinoviev.lambda.composition;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

/**
 * @author alexej_barzykin@epam.com
 */
public class FibonaciDemo {
    public static void main(String[] args) {
        List<Integer> fibonacciNumber = Arrays.asList(1, 1, 2, 3, 5, 8, 13);
        Function<Integer, Integer> addTwo = e -> e + 2;
        Function<Integer, Integer> multipleTen = e -> e * 10;

        fibonacciNumber.stream()
                .map(addTwo.andThen(multipleTen))
                .forEach(System.out::println);

        System.out.println();

        fibonacciNumber.stream()
                .map(addTwo.compose(multipleTen))
                .forEach(System.out::println);

        System.out.println();

        fibonacciNumber.stream()
                .map(compose(addTwo, multipleTen))
                .forEach(System.out::println);
    }

    private static <Integer> Function<Integer, Integer> compose(Function<Integer, Integer> x, Function<Integer, Integer> y) {
        return result -> y.apply(x.apply(result));
    }
}

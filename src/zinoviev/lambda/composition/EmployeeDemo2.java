package zinoviev.lambda.composition;

import zinoviev.lambda.composition.beans.Department;
import zinoviev.lambda.composition.beans.Manager;

import java.util.Collections;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.DoubleUnaryOperator;
import java.util.function.Function;

/**
 * @author alexej_barzykin@epam.com
 */
public class EmployeeDemo2 {
//    public static void main(String[] args) {
//        Function<String, Department> constructor = name -> new Department(name);
//        Function<String, Department> constructor2 = Department::new;
//
//        BiFunction<String, String, Manager> managerConstructor = Manager::new;
//        System.out.println(managerConstructor.apply("Ivan", "Petrovich"));
//
//        TriFunction<String, String, Integer, Manager> managerAgeConstructor = Manager::new;
//        System.out.println(managerAgeConstructor.apply("Victor", "Ivanovich", 57));
//
//        List.of("BackEnd", "FrontEnd").stream()
//                .map(constructor2)
//                .forEach(System.out::println);
//        BiFunction<String, CharSequence, Boolean> contains = String::contains;
//        System.out.println(contains.apply("Russia", "si"));
//
//        final DoubleUnaryOperator module = Math::abs;
//        System.out.println(module.applyAsDouble(-3.33));
//
//        Function<Department, String> getName = Department::getName;
//        System.out.println(getName.apply(constructor2.apply("Mobile")));
//
//    }
//
//    private static boolean isEven(Integer number) {
//        return number % 2 == 0;
//    }
//
//    @FunctionalInterface
//    private interface TriFunction<T, U, V, R> {
//        R apply(T t, U u, V v);
//    }
}

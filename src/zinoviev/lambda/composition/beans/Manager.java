package zinoviev.lambda.composition.beans;

/**
 * @author alexej_barzykin@epam.com
 */
public class Manager {
    private String name;
    private String middleName;
    private int age;

    public Manager(String name, String middleName) {
        this.name = name;
        this.middleName = middleName;
    }

    public Manager(String name, String middleName, int age) {
        this.name = name;
        this.middleName = middleName;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Manager{" +
                "name='" + name + '\'' +
                ", middleName='" + middleName + '\'' +
                ", age=" + age +
                '}';
    }
}

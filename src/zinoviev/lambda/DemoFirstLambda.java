package zinoviev.lambda;

/**
 * @author alexej_barzykin@epam.com
 */
public class DemoFirstLambda {
    public static void main(String[] args) {
        executeTask(() -> {
            new BusinessTaskUpdateCustomerAge()
                    .updateStateInDB();
            try {
                Thread.sleep(1000);
                System.out.println("MassiveUpdates");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        executeTask(() -> {
            new BusinessTask() {
                @Override
                public void updateStateInDB() {

                }
             }.updateStateInDBByDefault();
        });

        executeTask(() -> {
            ((BusinessTask)() -> {

            }).updateStateInDBByDefault();
        });

        executeTask(((BusinessTask) () -> {})::updateStateInDBByDefault);

    }

    private static void executeTask(Runnable threadLogic) {
        new Thread(threadLogic).start();
    }
}

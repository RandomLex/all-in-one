package zinoviev.lambda;

import java.util.stream.IntStream;

/**
 * @author alexej_barzykin@epam.com
 */
public class StreamDemo3 {
    public static void main(String[] args) {
        IntStream.iterate(0,i -> i + 2)
                .forEach(e -> {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                    System.out.println(e);
                });
        System.out.println("It won't be printed.");
    }
}

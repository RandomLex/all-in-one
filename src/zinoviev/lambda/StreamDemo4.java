package zinoviev.lambda;

import java.util.Arrays;

/**
 * @author alexej_barzykin@epam.com
 */
public class StreamDemo4 {
    public static void main(String[] args) {
        Arrays.asList(2, 3, 4.17788, 14, -2, 9.9)
                .stream()
                .sorted()
                .forEach(System.out::println);
    }
}

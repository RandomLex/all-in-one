package zinoviev.lambda;

import zinoviev.lambda.beans.Employee;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author alexej_barzykin@epam.com
 */
public class JoiningDemo {
    public static void main(String[] args) {
        List<Employee> employees = BusinessTaskUpdateCustomerAge.hireEmployeees();
        String names = employees.stream()
                .map(Employee::getName)
                .collect(Collectors.joining(";"));
        System.out.println(names);
    }
}

package zinoviev.lambda;

import zinoviev.lambda.beans.Department;
import zinoviev.lambda.beans.Employee;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.mapping;

/**
 * @author alexej_barzykin@epam.com
 */
public class Comparing {
    public static void main(String[] args) {
        List<Employee> employees = BusinessTaskUpdateCustomerAge.hireEmployeees();


        Map<Department, Long> result = employees.stream()
                .collect(groupingBy(Employee::getDepartment, counting()));
        System.out.println(result);

        Map<Department, String> result2 = employees.stream()
                .collect(groupingBy(Employee::getDepartment, mapping(Employee::getSkill, joining(" & "))));
        System.out.println(result2);
    }
}

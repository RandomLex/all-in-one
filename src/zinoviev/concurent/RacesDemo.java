package zinoviev.concurent;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author alexej_barzykin@epam.com
 */
public class RacesDemo {
    private int counter = 0;

    public static void main(String[] args) throws InterruptedException {
        RacesDemo demo = new RacesDemo();

        Lock lock = new ReentrantLock();

        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 10_000; i++) {
                lock.lock();
                try {
                    demo.counter = demo.counter + 1;
                } finally {
                    lock.unlock();
                }


            }
        });

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 10_000; i++) {
                lock.lock();
                try {
                    demo.counter = demo.counter - 1;
                } finally {
                    lock.unlock();
                }
            }
        });

        t1.start();
        t2.start();

        t1.join();
        t2.join();

        System.out.println("Counter = " + demo.counter);
    }
}

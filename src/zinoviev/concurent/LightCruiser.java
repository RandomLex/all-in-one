package zinoviev.concurent;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author alexej_barzykin@epam.com
 */
public class LightCruiser {
    private int health = 100;
    private boolean isDamaged;
    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

    public void repair() {
        ReentrantReadWriteLock.ReadLock readLock = lock.readLock();
        readLock.lock();
        if (isDamaged) {
            readLock.unlock();
            ReentrantReadWriteLock.WriteLock writeLock = lock.writeLock();
            writeLock.lock();
            health++;
            isDamaged = false;
            writeLock.unlock();
        } else {
            readLock.unlock();
        }
    }

    public void underShooting() {
        ReentrantReadWriteLock.WriteLock writeLock = lock.writeLock();
        writeLock.lock();
        health -= 10;
        isDamaged = true;
        writeLock.unlock();
    }

    public int getHealth() {
        ReentrantReadWriteLock.ReadLock readLock = lock.readLock();
        readLock.lock();
        int result = health;
        readLock.unlock();
        return result;
    }
}

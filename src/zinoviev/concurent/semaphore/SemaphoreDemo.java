package zinoviev.concurent.semaphore;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * @author alexej_barzykin@epam.com
 */
public class SemaphoreDemo {

    public static void main(String[] args) {
        Semaphore writingSemaphore = new Semaphore(1);
        Semaphore readingSemaphore = new Semaphore(10);

        List<Thread> threads = new ArrayList<>();
        List<Object> resources = new ArrayList<>();

        for (int i = 0; i < 100; i++) {
            Thread writer = new Thread(() -> {
                try {
                    System.out.println(threadName() + " is waiting for writing permits");
                    writingSemaphore.acquire();
                    System.out.println(threadName() + " has writing permits");
                    resources.add(new Object());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    writingSemaphore.release();
                }
            });
            writer.setName("Writer # " + i);
            threads.add(writer);
        }

        for (int i = 0; i < 100; i++) {
            Thread reader = new Thread(() -> {
                try {
                    System.out.println(threadName() + " is waiting for reading permits");
                    readingSemaphore.acquire();
                    System.out.println(threadName() + " has reading permits");
                    System.out.println("Number of elements: " + resources.size());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    readingSemaphore.release();
                }
            });
            reader.setName("Reader # " + i);
            threads.add(reader);
        }

        threads.forEach(Thread::start);

        threads.forEach(thread -> {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        System.out.println("Result: " + resources.size());
    }

    private static String threadName() {
        return Thread.currentThread().getName();
    }
}

package zinoviev.concurent;

/**
 * @author alexej_barzykin@epam.com
 */
public class ThreadDemo {
    public static void main(String[] args) throws InterruptedException {
        LightCruiser cruiser = new LightCruiser();

        Runnable japanFleet = () -> {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    System.out.println("ERROR with japanFleet " + e.getMessage());
                }
                cruiser.underShooting();
                System.out.println("Shooted. Remain:  " + cruiser.getHealth());
            }
        };
        Runnable vladivostok = () -> {
            while (!Thread.currentThread().isInterrupted()) {
                if (cruiser.getHealth() < 50) {
                    for (int i = 0; i < 9; i++) {
                        System.out.println("Repairing " + cruiser.getHealth());
                        try {
                            Thread.sleep(20);
                        } catch (InterruptedException e) {
                            System.out.println("ERROR with vladivostok " + e.getMessage());
                        }
                        cruiser.repair();
                    }
                }
            }
        };
        Thread t1 = new Thread(japanFleet);
        Thread t2 = new Thread(vladivostok);

        t1.start();
        t2.start();
        Thread.sleep(1000);
        t1.interrupt();
        t2.interrupt();
        t1.join();
        t2.join();

    }
}

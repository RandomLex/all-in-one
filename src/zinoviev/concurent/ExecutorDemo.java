package zinoviev.concurent;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author alexej_barzykin@epam.com
 */
public class ExecutorDemo {

    private static final int TIME_TO_WORK = 10_000;

    public static void main(String[] args) {
        ExecutorService service = Executors.newFixedThreadPool(10);

        Runnable taskOne = () -> {
            try {
                Thread.sleep(TIME_TO_WORK);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Hello from taskOne " + threadName());
        };

        Runnable taskTwo = () -> {
            try {
                Thread.sleep(TIME_TO_WORK);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Hello from taskTwo " + threadName());
        };

        service.submit(taskOne);
        service.submit(taskTwo);

        try {
            service.shutdown();
            service.awaitTermination(1, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            List<Runnable> runnables = service.shutdownNow();
            runnables.forEach(service::submit);
        }

        System.out.println("Hello from main " + threadName());
    }

    private static String threadName() {
        return Thread.currentThread().getName();
    }
}

package zinoviev.concurent;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author alexej_barzykin@epam.com
 */
public class ScheduledDemo {
    public static void main(String[] args) {
        ExecutorService service = Executors.newScheduledThreadPool(1);
        ((ScheduledExecutorService) service).schedule(() ->{
            System.out.println("Something");
        }, 2, TimeUnit.SECONDS);

        ((ScheduledExecutorService) service).scheduleAtFixedRate(() -> {
            System.out.println("Ping yandex");
        }, 0, 2, TimeUnit.SECONDS);

        ((ScheduledExecutorService) service).scheduleWithFixedDelay(() -> {
            System.out.println("Ping amazon");
        }, 3, 2, TimeUnit.SECONDS);

        try {
            service.awaitTermination(15, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            service.shutdownNow();
        }
    }
}

package equiv;

import com.google.common.base.Equivalence;
import com.google.common.base.Equivalence.Wrapper;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;

import static equiv.EquivUtils.PART_EQUIVALENCE_BY_NAME;
import static equiv.EquivUtils.SECTION_EQUIVALENCE_BY_ID;
import static equiv.EquivUtils.getListFromNewElements;

/**
 * @author alexej_barzykin@epam.com
 */
public class RunnerRemove {
    public static void main(String[] args) {
        List<Part> partListOne = getParts("111", "_111", "222", "_222", "333", "_XXX");
        List<Part> partListTwo = getParts("444", "_444", "555", "_555", "333", "_333");

        List<Section> sectionListOne = getSections("001", 100, "002", 200, "003", 300);
        List<Section> sectionListTwo = getSections("004", 400, "005", 500, "003", 600);

        List<Part> resultParts = getListFromNewElements(partListOne, partListTwo, PART_EQUIVALENCE_BY_NAME);

        System.out.println("-----------PART--------------");
        for (Part resultPart : resultParts) {
            System.out.println(resultPart);
        }

        List<Section> resultSections = getListFromNewElements(sectionListOne, sectionListTwo, SECTION_EQUIVALENCE_BY_ID);

        System.out.println("-----------SESSION--------------");
        for (Section resultSection : resultSections) {
            System.out.println(resultSection);
        }
    }

    private static List<Section> getSections(String id1, int capacity1, String id2, int capacity2, String id3, int capacity3) {
        Section sectionOne = new Section();
        sectionOne.setId(id1);
        sectionOne.setCapacity(capacity1);
        Section sectionTwo = new Section();
        sectionTwo.setId(id2);
        sectionTwo.setCapacity(capacity2);
        Section sectionThree = new Section();
        sectionThree.setId(id3);
        sectionThree.setCapacity(capacity3);
        List<Section> sectionListOne = Lists.newArrayList();
        sectionListOne.add(sectionOne);
        sectionListOne.add(sectionTwo);
        sectionListOne.add(sectionThree);
        return sectionListOne;
    }

    private static List<Part> getParts(String name1, String value1, String name2, String value2, String name3, String value3) {
        Part partOne = new Part();
        partOne.setName(name1);
        partOne.setValue(value1);
        Part partTwo = new Part();
        partTwo.setName(name2);
        partTwo.setValue(value2);
        Part partThree = new Part();
        partThree.setName(name3);
        partThree.setValue(value3);
        List<Part> partListOne = new ArrayList<Part>();
        partListOne.add(partOne);
        partListOne.add(partTwo);
        partListOne.add(partThree);
        return partListOne;
    }

    private static List<Wrapper<Part>> createPartWrapperList(List<Part> partListOne, Equivalence<Part> eq) {
        List<Wrapper<Part>> onePartEquivalenceList = Lists.newArrayList();
        for (Part part : partListOne) {
            Wrapper<Part> wrapper = eq.wrap(part);
            onePartEquivalenceList.add(wrapper);
        }
        return onePartEquivalenceList;
    }

    private static List<Wrapper<Section>> createSectionWrapperList(List<Section> sectionListOne, Equivalence<Section> sectionEq) {
        List<Wrapper<Section>> oneSectionWrapperList = Lists.newArrayList();
        for (Section section : sectionListOne) {
            Wrapper<Section> wrapper = sectionEq.wrap(section);
            oneSectionWrapperList.add(wrapper);
        }
        return oneSectionWrapperList;
    }






}

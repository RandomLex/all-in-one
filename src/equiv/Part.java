package equiv;

/**
 * @author alexej_barzykin@epam.com
 */
public class Part {
    private String name;
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Part)) return false;

        Part part = (Part) o;

        if (getName() != null ? !getName().equals(part.getName()) : part.getName() != null) return false;
        return getValue() != null ? getValue().equals(part.getValue()) : part.getValue() == null;
    }

    @Override
    public int hashCode() {
        int result = getName() != null ? getName().hashCode() : 0;
        result = 31 * result + (getValue() != null ? getValue().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Part{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
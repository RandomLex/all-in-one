package equiv;

import com.google.common.base.Equivalence;
import com.google.common.base.Equivalence.Wrapper;
import com.google.common.base.Objects;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toList;

/**
 * @author alexej_barzykin@epam.com
 */
public final class EquivUtils {

    public static final Equivalence<Part> PART_EQUIVALENCE_BY_NAME = new PartEquivalence();
    public static final Equivalence<Section> SECTION_EQUIVALENCE_BY_ID = new SectionEquivalence();

    private EquivUtils() {}

    public static <T> List<T> getListFromNewElements(List<T> left, List<T> right, Equivalence<T> strategy) {
        List<Wrapper<T>> wrappedLeft = getWrappedList(left, strategy);
        List<Wrapper<T>> wrappedRight = getWrappedList(right, strategy);

        wrappedLeft.removeAll(wrappedRight);

        List<T> resultList = newArrayList();
        for (Wrapper<T> wrappedElement : wrappedLeft) {
            resultList.add(wrappedElement.get());
        }
        return resultList;
    }

    public static <T> List<Wrapper<T>> getWrappedList(List<T> list, Equivalence<T> strategy) {
        return list.stream()
                .map(strategy::wrap)
                .collect(toList());
    }

    private static class SectionEquivalence extends Equivalence<Section> {
        @Override
        protected boolean doEquivalent(Section left, Section right) {
            return Objects.equal(left.getId(), right.getId());
        }

        @Override
        protected int doHash(Section section) {
            return section.getId().hashCode();
        }
    }

    private static class PartEquivalence extends Equivalence<Part> {
        @Override
        protected boolean doEquivalent(Part left, Part right) {
            return Objects.equal(left.getName(), right.getName());
        }

        @Override
        protected int doHash(Part part) {
            return Objects.hashCode(part);
        }
    }

}
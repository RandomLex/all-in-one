package equiv;

/**
 * @author alexej_barzykin@epam.com
 */
public class Section {
    private String id;
    private int capacity;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Section)) return false;

        Section section = (Section) o;

        if (getCapacity() != section.getCapacity()) return false;
        return getId() != null ? getId().equals(section.getId()) : section.getId() == null;
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + getCapacity();
        return result;
    }

    @Override
    public String toString() {
        return "Section{" +
                "id='" + id + '\'' +
                ", capacity=" + capacity +
                '}';
    }
}
package jr;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Scanner;

/**
 * Округление чисел
 Считать с консоли 2 имени файла.
 Первый файл содержит вещественные(дробные) числа, разделенные пробелом. Например, 3.1415.
 Округлить числа до целых и записать через пробел во второй файл.
 Закрыть потоки.

 Принцип округления:
 3.49 — 3
 3.50 — 4
 3.51 — 4
 -3.49 — -3
 -3.50 — -3
 -3.51 — -4


 Требования:
 1. Программа должна два раза считать имена файлов с консоли.
 2. Для первого файла создай поток для чтения. Для второго - поток для записи.
 3. Считать числа из первого файла, округлить их и записать через пробел во второй.
 4. Должны соблюдаться принципы округления, указанные в задании.
 5. Созданные для файлов потоки должны быть закрыты.
 *
 * @author alexej_barzykin@epam.com
 */
public class Solution {
    public static void main(String[] args) throws IOException {
        double d = -3.51;


//        Scanner sc = new Scanner(System.in);
//
//        BufferedInputStream in = new BufferedInputStream(new FileInputStream(sc.nextLine()));
//        BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(sc.nextLine()));
//
//        StringBuilder sb = new StringBuilder();
//        while (in.available() > 0) {
//            int c = in.read();
//            if (c != ' ' && in.available() > 0) {
//                sb.append((char) c);
//            }
//            else {
//                long rounded = Math.round(Double.parseDouble(sb.toString()));
//                String.valueOf(rounded).chars().forEach(i -> {
//                    try {
//                        out.write(i);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                });
//                if (in.available() > 0) {
//                    out.write(' ');
//                }
//                sb = new StringBuilder();
//            }
//        }
//        out.close();
//        in.close();
    }
}

package methodasparam;

import java.util.function.Consumer;

public class MethodAsParamDemoResult {
    public static void main(String[] args) {
        String header = "******";
        String footer = "------";
        String body = "BoDy";
        printFormatted(header, footer, body, MethodAsParamDemoResult::printUpper);
        printFormatted(header, footer, body, MethodAsParamDemoResult::printLower);
    }

    private static void printFormatted(String header, String footer, String body, Consumer<String> formatter) {
        System.out.println(header);
        formatter.accept(body);
        System.out.println(footer);
    }

    private static void printUpper(String str) {
        System.out.println(str.toUpperCase());
    }

    private static void printLower(String str) {
        System.out.println(str.toLowerCase());
    }
}

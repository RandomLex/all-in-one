package methodasparam;

public class MethodAsParamDemoSource {
    public static void main(String[] args) {
        String header = "******";
        String footer = "------";
        String body = "BoDy"; // mix of Upper and Lower cases
        printUpper(header, footer, body);
        printLower(header, footer, body);
    }

    private static void printUpper(String header, String footer, String body) {
        System.out.println(header);
        System.out.println(body.toUpperCase());
        System.out.println(footer);
    }

    private static void printLower(String header, String footer, String body) {
        System.out.println(header);
        System.out.println(body.toLowerCase());
        System.out.println(footer);
    }
}

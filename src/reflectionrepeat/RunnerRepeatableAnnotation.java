package reflectionrepeat;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author alexej_barzykin@epam.com
 */
public class RunnerRepeatableAnnotation {
  public static void main(String[] args) {
    Box shirt = new Box();
    getColours(shirt)
            .forEach(System.out::println);
  }

  private static List<String> getColours(Object o) {
    Colour[] colours = o.getClass().getAnnotationsByType(Colour.class);
    System.out.println("!!!");
    return Arrays.stream(colours)
            .map(Colour::name)
            .collect(Collectors.toList());
  }
}

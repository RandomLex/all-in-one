package reflectionrepeat;

import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author alexej_barzykin@epam.com
 */
@Repeatable(Colour.List.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface Colour {
  String name();

  @Retention(RetentionPolicy.RUNTIME)
  @interface List {
    Colour[] value();
  }
}

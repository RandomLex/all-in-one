/**
 * @author alexej_barzykin@epam.com
 */
public class StaticNonStatic {
  static int staticOne;
  int nonStaticOne;
  public static void main(String[] args) {
    StaticNonStatic obj1 = new StaticNonStatic();
    StaticNonStatic obj2 = new StaticNonStatic();
    staticOne = 3;
    ExternalClass.handle(obj1);
    ExternalClass.handle(obj2);
  }
}

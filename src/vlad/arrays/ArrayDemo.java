package vlad.arrays;

import java.net.SocketTimeoutException;

/**
 * @author alexej_barzykin@epam.com
 */
public class ArrayDemo {
    public static void main(String[] args) {
        int[] ints = new int[4];
        int[] aArray = {3, 4, 8, -10};
        int[] bArray = new int[] {3, 4, 6};
        String[] strArray = {"AA", "BB", "CC"};

        String abc = "asdufoaifuhasf";

        printArray(aArray);

        int x = 10;
        print(x);
        print(15);

        printArray(new int [] {3, 4, 8, -10});

        int[][] matrix = new int[3][3];
        int[][][] cube = new int[3][3][3];

        int[] row1 = {1, 2, 3};
        int[] row2 = {7, 14, 6};
        int[] row3 = {26, 9, 12};

        int[][] m = {row1, row2, row3};

        m[0] = row1;

        int[][] n = {{1, 2},
                     {4, 5, 6, 8, 9},
                     {7, 8, 9}};


        int max = m[0][0];
        int xCoord = 0;
        int yCoord = 0;
        System.out.print("Главная диагональ: ");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (m[i][j] > max) {
                    max = m[i][j];
                    yCoord = i;
                    xCoord = j;
                }
                if (i == j) {
                    System.out.print(m[i][j] + ", ");
                }
            }
        }
        System.out.println();
        System.out.println("m[" + xCoord + "][" + yCoord + "] = " + max);

    }

    public static void print(int i) {
        System.out.println(i);
    }

    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + ", ");
        }
        System.out.println();
    }
}

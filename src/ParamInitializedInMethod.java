/**
 * @author alexej_barzykin@epam.com
 */
public class ParamInitializedInMethod {
  public static void main(String[] args) {
    ParamInitializedInMethod test = new ParamInitializedInMethod();
    SomeObj some = null;
    test.change(some);
    System.out.println(some);

  }

  private void change(SomeObj someObj) {
    someObj = new SomeObj();
    someObj.setValue(100);
  }

  private static class SomeObj {
    private int value = 42;

    public int getValue() {
      return value;
    }

    public void setValue(int value) {
      this.value = value;
    }
  }
}

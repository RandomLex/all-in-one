import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.Date;

/**
 * @author alexej_barzykin@epam.com
 */
public class LocalDateTimeFormatter {
  private static final DateTimeFormatter TIME_FORMATTER
//          = DateTimeFormatter.ofPattern("yyyy-MM-dd");
          = new DateTimeFormatterBuilder()
          .appendPattern("yyyy-MM-dd hh:mm:ss")
          .toFormatter();

  public static void main(String[] args) {
//    Date date = str2Date("2019-08-30", TIME_FORMATTER);
//    System.out.println(date);

    DateTimeFormatter DATE_FORMAT =
            new DateTimeFormatterBuilder().appendPattern("yyyy-MM-dd")
                    .parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
                    .parseDefaulting(ChronoField.MINUTE_OF_HOUR, 0)
                    .parseDefaulting(ChronoField.SECOND_OF_MINUTE, 0)
                    .toFormatter();
    String dateStr = "2016-01-01";
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");
    LocalDate localDate = LocalDate.parse(dateStr, DATE_FORMAT);
//    LocalDateTime localDateTime = LocalDateTime.parse(dateStr, DATE_FORMAT);
    System.out.println(localDate.atStartOfDay());
  }

  private static Date str2Date(String dateAsString, DateTimeFormatter formatter) {
    LocalDate dateToConvert = LocalDate.parse(dateAsString, formatter);
    return Date.from(dateToConvert.atStartOfDay(ZoneId.systemDefault()).toInstant());
  }
}

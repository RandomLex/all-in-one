import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author alexej_barzykin@epam.com
 */
public class CheckListSort {
    public static void main(String[] args) {
        List<String> stringList = new ArrayList<>();

        LinkedHashMap<Integer, String> map = new LinkedHashMap<>();
        map.put(10, "AAA");
        map.put(9, "999");
        map.put(3, "333");
        map.put(7, "777");

        for (Map.Entry<Integer, String> integerStringEntry : map.entrySet()) {
            System.out.println(integerStringEntry.getKey() + " " + integerStringEntry.getValue());
        }

    }
}

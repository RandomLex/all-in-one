import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author alexej_barzykin@epam.com
 */
public class ConvertTime {

    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.nnnnnnnnn").withZone(ZoneOffset.UTC);

    public static void main(String[] args) {
        String in = "2018-07-09 14:18:52.000000000";
        String zone = "GMT+4";
        System.out.println(sqlTimeToUI(in));
    }

    public static String sqlTimeToUI(String sqlTime) {
        return ZonedDateTime.parse(sqlTime, TIME_FORMATTER).toString();

    }

}

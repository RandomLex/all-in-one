package matrix_muti;

import java.util.Arrays;

public class MatrixMultiply {
    public static void main(String[] args) {
        //expected result:
//      17, 11
//      25, 13

        int[][] a = {
                {3, 2, 1},
                {3, 4, 5}
        };
        int[][] b = {
                {4, 3},
                {2, 1},
                {1, 0}
        };
        System.out.println(Arrays.deepToString(a));
        System.out.println(Arrays.deepToString(b));
        System.out.println(Arrays.deepToString(multi(a, b)));
    }

    private static int[][] multi(int[][] matrixA, int[][] matrixB) {
        int[][] result = new int[matrixA.length][matrixB[0].length];


        for (int resultRow = 0; resultRow < result.length; resultRow++) {
            for (int resultColumn = 0; resultColumn < result[0].length; resultColumn++) {
                for (int i = 0; i < matrixA[0].length; i++) {
                    result[resultRow][resultColumn] += matrixA[resultRow][i] * matrixB[i][resultColumn];
                }
            }
        }
        return result;
    }
}

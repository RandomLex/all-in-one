package cycle_shift;

import java.util.Arrays;

public class CycleShift {
    public static void main(String[] args) {
//        int[] input = {-18, 21, 3, 6, 7, 65};
//        int[] input = {18, 6, 7, 3, 6, 9, 3, 5, 7, 8};
        int[] input = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
//        int[] input = {18, 6};
//        int[] input = {6, 18};
//        int[] input = {1, 2, 3, 4, 5};
//        int[] input = {5, 4, 3, 2, 1};
//        int[] input = {1, 1, 1, 1, 1};

        System.out.println(Arrays.toString(input));

        System.out.println(Arrays.toString(cycleShift(input, 3)));
        System.out.println(Arrays.toString(cycleShift(input)));
    }

    private static int[] cycleShift(int[] input) {
        return cycleShift(input, 1);
    }

    private static int[] cycleShift(int[] input, int shift) {
        int[] result = new int[input.length];
        System.arraycopy(input, 0, result, shift, input.length - shift);
        System.arraycopy(input, input.length - shift, result, 0, shift);
        return result;
    }

}

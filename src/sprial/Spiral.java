package sprial;


public class Spiral {
    public static void main(String[] args) {
        int[][] spiral = spiral(5, 5);

        for (int i = 0; i < spiral.length; i++) {
            for (int j = 0; j < spiral[i].length; j++) {
                System.out.printf("%4d", spiral[i][j]);
            }
            System.out.println();
        }
    }

    private static int[][] spiral(int a, int b) {
        int[][] matrix = new int[a][b];

        int left = 0;
        int right = b - 1;
        int top = 0;
        int bottom = a - 1;
        int i = 0;
        int j = 0;
        int di = 0;
        int dj = 0;

        for (int counter = 1; counter <= a * b; counter++) {
            matrix[i+=di][j+=dj] = counter;
            if (i == top && j == left) {
                di = 0;
                dj = 1;
            } else if (i == top && j == right) {
                di = 1;
                dj = 0;
            } else if (i == bottom && j == right) {
                di = 0;
                dj = -1;
            } else if (i == bottom && j == left) {
                di = -1;
                dj = 0;
            }
            if (counter > 1 && i == top + 1 && j == left) {
                left++; right--; top++; bottom--;
                di = 0; dj = 1;
            }

        }
        return matrix;
    }
}

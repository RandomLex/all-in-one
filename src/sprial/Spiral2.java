package sprial;


public class Spiral2 {
    public static void main(String[] args) {
        int[][] spiral = spiral(3, 5);

        for (int i = 0; i < spiral.length; i++) {
            for (int j = 0; j < spiral[i].length; j++) {
                System.out.printf("%4d", spiral[i][j]);
            }
            System.out.println();
        }
    }

    private static int[][] spiral(int a, int b) {
        int[][] matrix = new int[a][b];

        int left = 0;
        int right = b - 1;
        int top = 0;
        int bottom = a - 1;

        for (int counter = 1; counter <= a * b; ) {
            for (int j = left; j <= right; j++) {
                matrix[top][j] = counter++;
            }
            top++;

            for (int i = top; i <= bottom; i++) {
                matrix[i][right] = counter++;
            }
            right--;

            if (counter > a * b) {
                break;
            }

            for (int j = right; j >= left; j--) {
                matrix[bottom][j] = counter++;
            }
            bottom--;

            for (int i = bottom; i >= top; i--) {
                matrix[i][left] = counter++;
            }
            left++;
        }
        return matrix;
    }
}

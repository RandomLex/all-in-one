package sorts.inset;

public class InsertSortDemo {
    public static void main(String[] args) {
        int[] array = {7, 3, 9, 0, 4, 6, 2};
//        int[] array = {7};
        System.out.println("Initial array");
        printArray(array);
        sort(array);
        System.out.println("Sorted array");
        printArray(array);
    }

    private static void sort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int key = array[i];
            int j = i - 1;
            while (j >= 0 && array[j] > key) {
                array[j + 1] = array[j];
                j--;
            }
            array[j + 1] = key;
        }
    }

    private static void printArray(int[] array) {
        for (int i : array) {
            System.out.print(i + " ");
        }
        System.out.println();
    }
}

package sorts.bubble;

public class BubbleSortDemo {
    public static void main(String[] args) {
//        int[] array = {7, 3, 9, 0, 4, 6, 2};
        int[] array = {7};
        System.out.println("Initial array");
        printArray(array);
        sort(array);
        System.out.println("Sorted array");
        printArray(array);
    }

    private static void sort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }

    private static void printArray(int[] array) {
        for (int i : array) {
            System.out.print(i + " ");
        }
        System.out.println();
    }
}

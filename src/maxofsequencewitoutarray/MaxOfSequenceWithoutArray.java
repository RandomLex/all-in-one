package maxofsequencewitoutarray;

import java.util.Scanner;

public class MaxOfSequenceWithoutArray {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int max = Integer.MIN_VALUE;
        int input;

        while (true) {
            input = sc.nextInt();
            if (input == 0) {
                break;
            }
            max = max(max, input);
        }

        System.out.println("Maximum is: " + max);
    }

    static int max(int a, int b) {
        return Math.max(a, b);
    }
}

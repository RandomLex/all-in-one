package bees.office;

import java.util.ArrayList;

/**
 * @author alexej_barzykin@epam.com
 */
public class Demo {
  public static void main(String[] args) {

    ArrayList<Office> officeMas = new ArrayList<>();
    Office office1 = new Office("Mobile", "Tech");
    officeMas.add(office1);
    Office office2 = new Office("Enterprise", "Tech");
    officeMas.add(office2);
    Office office3 = new Office("Accountant", "Finance");
    officeMas.add(office3);

    ArrayList<Office> techOffices = filterOfficeByType(officeMas, "Tech");

    for (Office office : techOffices) {
      office.officeInfo();
    }

  }

  static ArrayList<Office> filterOfficeByType(ArrayList<Office> offices, String type) {
    ArrayList<Office> result = new ArrayList<>();
    for (Office office : offices) {
      if (type.equals(office.type)) {
        result.add(office);
      }
    }
    return result;
  }

}

package bees.office;

import java.util.Scanner;

/**
 * @author alexej_barzykin@epam.com
 */
public class Office {
  public String title;
  public String type;

  public Office(String title, String type) {
    this.title = title;
    this.type = type;
  }

  public void officeTitleCreate() {
    System.out.println("введите название отдела");
    Scanner scan = new Scanner(System.in);
    title = scan.nextLine();
  }
  public void officeTypeCreate() {
    System.out.println("введите тип отдела");
    Scanner scan1 = new Scanner(System.in);
    type = scan1.nextLine();
  }

  public void officeInfo () {

    System.out.println("название отдела: " + title + ", тип отдела: " + type);
  }

}

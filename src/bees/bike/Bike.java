package bees.bike;

import java.util.ArrayList;

import java.util.ArrayList;
import java.util.Collections;

public class Bike {

  public String firm;
  public String type;
  public String speed;
  private static int mount = 0;
  private static int road = 0;

  Bike(String firm, String type, String speed) {
    this.firm = firm;
    this.type = type;
    this.speed = speed;
    if (this.type.equals("горный")) {
      mount++;
    }
    else if (this.type.equals("дорожный")){
      road++;
    }

  }
        /*public void setFirm (String firm){
            this.firm = firm;
        }

        public void setSpeed (String speed){
            this.speed = speed;
        }*/

  public void printNumberMount () {
    System.out.println("количество горных " + mount);
  }

  public void printNumberRoad () {
    System.out.println("количество дорожных " + road);
  }

  public int returnNumberMount () {
    return (mount);
  }

  public int returnNumberRoad () {
    return (road);
  }

  public void printBike (ArrayList < Bike > a) {
    int length = a.size();
    for (int i = 0; i < length; i++) {
      System.out.println(a.get(i).firm + "  " + a.get(i).type + "  " + a.get(i).speed);
      System.out.println();
    }
  }
}
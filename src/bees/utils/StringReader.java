package bees.utils;

import java.util.Scanner;

public class StringReader {
  private Scanner scan = new Scanner(System.in);

  public String read(String hint){
    System.out.println(hint);
    String result = scan.nextLine();
    return result;
  }
}
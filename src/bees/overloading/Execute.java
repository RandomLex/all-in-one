package bees.overloading;

/**
 * @author alexej_barzykin@epam.com
 */
public class Execute {
  public static void main(String[] args) {

    String[][] array1 = new String[3][4];
    String[][] array2 = new String[3][4];
    Float[][] array3;
    Integer[][] array4;
    CreateMassive massive = new CreateMassive();
    array1 = massive.doArray();
        /*array1[0][0] = "1.23";
        array1[0][1] = "5.88";
        array1[0][2] = "2.44";
        array1[0][3] = "3.5";
        array1[1][0] = "6.22";
        array1[1][1] = "7.11";
        array1[1][2] = "2.22";
        array1[1][3] = "6.88";
        array1[2][0] = "3.11";
        array1[2][1] = "9.66";
        array1[2][2] = "245.22";
        array1[2][3] = "1.101";*/
    massive.printArray(array1);

    //array2  massive.doArray();
    array2[0][0] = "1.23";
    array2[0][1] = "5.88";
    array2[0][2] = "2.44";
    array2[0][3] = "3.5";
    array2[1][0] = "6.22";
    array2[1][1] = "7.11";
    array2[1][2] = "2.22";
    array2[1][3] = "6.88";
    array2[2][0] = "3.11";
    array2[2][1] = "9.66";
    array2[2][2] = "245.22";
    array2[2][3] = "1.101";

    array3 = massive.convertArray(array2);
    massive.printArray(array3);
//    array4 = massive.doArray();
//    array4 = massive.convertArray(array3);
//    massive.printArray(array4);
  }
}
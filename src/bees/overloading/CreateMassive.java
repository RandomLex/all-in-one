package bees.overloading;


import bees.utils.StringReader;

/**
 * @author alexej_barzykin@epam.com
 */
public class CreateMassive {
  private StringReader reader = new StringReader();


  public String[][] doArray() {

    String[][] array = new String[3][4];
    for (int i = 0; i < array.length; i++) {
      for (int j = 0; j < array[i].length; j++) {
        array[i][j] = reader.read("Введите элемент array[" + i + "][" + j + "]:");
      }
    }
    return (array);
  }

  public Float[][] convertArray(String[][] a) {

    Float[][] floatArray = new Float[3][4];
    for (int i = 0; i < a.length; i++) {
      for (int j = 0; j < a[i].length; j++) {
        floatArray[i][j] = Float.valueOf(a[i][j]);
      }
    }
    return (floatArray);
  }

//   public Integer[][] convertArray(Float[][] b) {
//
//        Integer[][] intArray = new Integer[3][4];
//        for (int i = 0; i < b.length; i++) {
//            for (int j = 0; j < b[i].length; j++) {
//                intArray[i][j] = Integer.valueOf(b[i][j]);
//            }
//        }
//        return (intArray);
//    }

  public void printArray (Object[][]a){

    for (int i = 0; i < a.length; i++) {
      for (int j = 0; j < a[i].length; j++) {
        System.out.print(a[i][j] + " ");
      }
      System.out.println();
    }
  }

}
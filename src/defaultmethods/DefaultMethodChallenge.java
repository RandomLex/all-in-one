package defaultmethods;

/**
 * @author alexej_barzykin@epam.com
 */
public class DefaultMethodChallenge {
  public static void main(String[] args) {
    Jofrey jofrey = new Jofrey() {
      @Override
      public void kill() {
        Jofrey.super.kill();
      }
    };
    jofrey.kill();

    Multiple multiple = new Multiple();
    multiple.kill();

  }

  static class Multiple implements Cersei, Stanis {
    @Override
    public void kill() {
      Cersei.super.kill();
      Stanis.super.kill();
    }

  }

  interface Cersei {
    default void kill() {
      System.out.println("cersei");
    }
  }

  interface Jofrey extends Cersei {
    @Override
    default void kill() {
      System.out.println("jofrey");
    }
  }

  interface Stanis {
    default void kill() {
      System.out.println("stanis");
    }
  }
}

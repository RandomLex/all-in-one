import java.util.Scanner;

/**
 * @author alexej_barzykin@epam.com
 */
public class DoroshenkoQuestion001 {
  public static void main(String[] args) {
    double a;

    Scanner scanner = new Scanner(System.in);
    a=scanner.nextDouble();
    while (a>5.0) {a=a-5;}
    if (a<3.0 && a>=1.0){
      System.out.println("зелёный");
    }
    else if (a>=3.0 && a<4.0){
      System.out.println("жёлтый");
    }
    else if (a>=4.0 && a<=5.0) {
      System.out.println("красный");
    }
  }
}

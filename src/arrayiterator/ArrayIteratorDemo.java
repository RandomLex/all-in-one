package arrayiterator;

public class ArrayIteratorDemo {
    public static void main(String[] args) {
        Integer[] ints = {0, 1, 2, 3};
        ArrayIterator<Integer> iterator = new ArrayIterator<>(ints);
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}

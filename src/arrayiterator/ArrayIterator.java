package arrayiterator;

import lombok.extern.slf4j.Slf4j;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayIterator<T> implements Iterator<T> {
    private final T[] array;
    private int pointer = 0;



    public ArrayIterator(T[] array) {
        this.array = array;
    }

    @Override
    public boolean hasNext() {
        return pointer < array.length;
    }

    @Override
    public T next() {
        if (hasNext()) {
            return array[pointer++];
        }
        throw new NoSuchElementException();
    }
}

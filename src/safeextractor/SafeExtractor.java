package safeextractor;

import java.util.Optional;
import java.util.function.Function;

/**
 * @author alexej_barzykin@epam.com
 */
public final class SafeExtractor {
    /**
     * Empty constructor.
     */
    private SafeExtractor() {
        // prevents the object creating
    }

    /**
     * Extracts target object from source object using one mapper function.
     *
     * @param <S>    Source type
     * @param <T>    Target type
     * @param source Source object
     * @param f1     Mapper
     * @return Target object.
     */
    public static <S, T> Optional<T> extract(S source,
                                             Function<S, T> f1) {
        return Optional.ofNullable(extract1(source, f1));
    }

    /**
     * Extracts target object from source object using two mapper functions.
     *
     * @param <S>    Source type
     * @param <T>    Target type
     * @param <T1>   Intermediate type
     * @param source Source object
     * @param f1     First mapper
     * @param f2     Second mapper
     * @return Target object.
     */
    public static <S, T, T1> Optional<T> extract(S source,
                                                 Function<S, T1> f1,
                                                 Function<T1, T> f2) {
        return Optional.ofNullable(extract2(source, f1, f2));
    }

    /**
     * Extracts target object from source object using three mapper functions.
     *
     * @param <S>    Source type
     * @param <T>    Target type
     * @param <T1>   Intermediate type
     * @param <T2>   Intermediate type
     * @param source Source object
     * @param f1     First mapper
     * @param f2     Second mapper
     * @param f3     Third mapper
     * @return Target object.
     */
    public static <S, T, T1, T2> Optional<T> extract(S source,
                                                     Function<S, T1> f1,
                                                     Function<T1, T2> f2,
                                                     Function<T2, T> f3) {
        return Optional.ofNullable(extract3(source, f1, f2, f3));
    }

    /**
     * Extracts target object from source object using four mapper functions.
     *
     * @param <S>    Source type
     * @param <T>    Target type
     * @param <T1>   Intermediate type
     * @param <T2>   Intermediate type
     * @param <T3>   Intermediate type
     * @param source Source object
     * @param f1     First mapper
     * @param f2     Second mapper
     * @param f3     Third mapper
     * @param f4     Fourth mapper
     * @return Target object.
     */
    public static <S, T, T1, T2, T3> Optional<T> extract(S source,
                                                         Function<S, T1> f1,
                                                         Function<T1, T2> f2,
                                                         Function<T2, T3> f3,
                                                         Function<T3, T> f4) {
        return Optional.ofNullable(extract4(source, f1, f2, f3, f4));
    }

    /**
     * Extracts target object from source object using five mapper functions.
     *
     * @param <S>    Source type
     * @param <T>    Target type
     * @param <T1>   Intermediate type
     * @param <T2>   Intermediate type
     * @param <T3>   Intermediate type
     * @param <T4>   Intermediate type
     * @param source Source object
     * @param f1     First mapper
     * @param f2     Second mapper
     * @param f3     Third mapper
     * @param f4     Fourth mapper
     * @param f5     Fifth mapper
     * @return Target object.
     */
    public static <S, T, T1, T2, T3, T4> Optional<T> extract(S source,
                                                             Function<S, T1> f1,
                                                             Function<T1, T2> f2,
                                                             Function<T2, T3> f3,
                                                             Function<T3, T4> f4,
                                                             Function<T4, T> f5) {
        return Optional.ofNullable(extract5(source, f1, f2, f3, f4, f5));
    }

    /**
     * Extracts target object from source object using six mapper functions.
     *
     * @param <S>    Source type
     * @param <T>    Target type
     * @param <T1>   Intermediate type
     * @param <T2>   Intermediate type
     * @param <T3>   Intermediate type
     * @param <T4>   Intermediate type
     * @param <T5>   Intermediate type
     * @param source Source object
     * @param f1     First mapper
     * @param f2     Second mapper
     * @param f3     Third mapper
     * @param f4     Fourth mapper
     * @param f5     Fifth mapper
     * @param f6     Sixth mapper
     * @return Target object.
     */
    public static <S, T, T1, T2, T3, T4, T5> Optional<T> extract(S source,
                                                                 Function<S, T1> f1,
                                                                 Function<T1, T2> f2,
                                                                 Function<T2, T3> f3,
                                                                 Function<T3, T4> f4,
                                                                 Function<T4, T5> f5,
                                                                 Function<T5, T> f6) {
        return Optional.ofNullable(extract6(source, f1, f2, f3, f4, f5, f6));
    }

    /**
     * Extracts target object from source object using seven mapper functions.
     *
     * @param <S>    Source type
     * @param <T>    Target type
     * @param <T1>   Intermediate type
     * @param <T2>   Intermediate type
     * @param <T3>   Intermediate type
     * @param <T4>   Intermediate type
     * @param <T5>   Intermediate type
     * @param <T6>   Intermediate type
     * @param source Source object
     * @param f1     First mapper
     * @param f2     Second mapper
     * @param f3     Third mapper
     * @param f4     Fourth mapper
     * @param f5     Fifth mapper
     * @param f6     Sixth mapper
     * @param f7     Seventh mapper
     * @return Target object.
     */
    public static <S, T, T1, T2, T3, T4, T5, T6> Optional<T> extract(S source,
                                                                     Function<S, T1> f1,
                                                                     Function<T1, T2> f2,
                                                                     Function<T2, T3> f3,
                                                                     Function<T3, T4> f4,
                                                                     Function<T4, T5> f5,
                                                                     Function<T5, T6> f6,
                                                                     Function<T6, T> f7) {
        return Optional.ofNullable(extract7(source, f1, f2, f3, f4, f5, f6, f7));
    }

    /**
     * Extracts target object from source object using provided mapper function.
     *
     * <p>Target is set to null if source is null or intermediate object obtained after applying mapper function is null.</p>
     *
     * @param <S>    Source type
     * @param <T>    Target type
     * @param source Source object
     * @param f      Mapper
     * @return Target object
     */
    private static <S, T> T extractNullSafe(S source, Function<S, T> f) {
        if (source != null) {
            return f.apply(source);
        }
        return null;
    }

    /**
     * Extracts target object from source object using one mapper function.
     *
     * @param <S>    Source type
     * @param <T>    Target type
     * @param source Source object
     * @param f1     Mapper
     * @return Target object.
     */
    private static <S, T> T extract1(S source, Function<S, T> f1) {
        return extractNullSafe(source, f1);
    }

    /**
     * Extracts target object from source object using two mapper functions.
     *
     * @param <S>    Source type
     * @param <T>    Target type
     * @param <T1>   Intermediate type
     * @param source Source object
     * @param f1     First mapper
     * @param f2     Second mapper
     * @return Target object.
     */
    private static <S, T, T1> T extract2(S source,
                                         Function<S, T1> f1,
                                         Function<T1, T> f2) {
        return extractNullSafe(extract1(source, f1), f2);
    }

    /**
     * Extracts target object from source object using three mapper functions.
     *
     * @param <S>    Source type
     * @param <T>    Target type
     * @param <T1>   Intermediate type
     * @param <T2>   Intermediate type
     * @param source Source object
     * @param f1     First mapper
     * @param f2     Second mapper
     * @param f3     Third mapper
     * @return Target object.
     */
    private static <S, T, T1, T2> T extract3(S source,
                                             Function<S, T1> f1,
                                             Function<T1, T2> f2,
                                             Function<T2, T> f3) {
        return extractNullSafe(extract2(source, f1, f2), f3);
    }

    /**
     * Extracts target object from source object using four mapper functions.
     *
     * @param <S>    Source type
     * @param <T>    Target type
     * @param <T1>   Intermediate type
     * @param <T2>   Intermediate type
     * @param <T3>   Intermediate type
     * @param source Source object
     * @param f1     First mapper
     * @param f2     Second mapper
     * @param f3     Third mapper
     * @param f4     Fourth mapper
     * @return Target object.
     */
    private static <S, T, T1, T2, T3> T extract4(S source,
                                                 Function<S, T1> f1,
                                                 Function<T1, T2> f2,
                                                 Function<T2, T3> f3,
                                                 Function<T3, T> f4) {
        return extractNullSafe(extract3(source, f1, f2, f3), f4);
    }

    /**
     * Extracts target object from source object using five mapper functions.
     *
     * @param <S>    Source type
     * @param <T>    Target type
     * @param <T1>   Intermediate type
     * @param <T2>   Intermediate type
     * @param <T3>   Intermediate type
     * @param <T4>   Intermediate type
     * @param source Source object
     * @param f1     First mapper
     * @param f2     Second mapper
     * @param f3     Third mapper
     * @param f4     Fourth mapper
     * @param f5     Fifth mapper
     * @return Target object.
     */
    private static <S, T, T1, T2, T3, T4> T extract5(S source,
                                                     Function<S, T1> f1,
                                                     Function<T1, T2> f2,
                                                     Function<T2, T3> f3,
                                                     Function<T3, T4> f4,
                                                     Function<T4, T> f5) {
        return extractNullSafe(extract4(source, f1, f2, f3, f4), f5);
    }

    /**
     * Extracts target object from source object using six mapper functions.
     *
     * @param <S>    Source type
     * @param <T>    Target type
     * @param <T1>   Intermediate type
     * @param <T2>   Intermediate type
     * @param <T3>   Intermediate type
     * @param <T4>   Intermediate type
     * @param <T5>   Intermediate type
     * @param source Source object
     * @param f1     First mapper
     * @param f2     Second mapper
     * @param f3     Third mapper
     * @param f4     Fourth mapper
     * @param f5     Fifth mapper
     * @param f6     Sixth mapper
     * @return Target object.
     */
    private static <S, T, T1, T2, T3, T4, T5> T extract6(S source,
                                                         Function<S, T1> f1,
                                                         Function<T1, T2> f2,
                                                         Function<T2, T3> f3,
                                                         Function<T3, T4> f4,
                                                         Function<T4, T5> f5,
                                                         Function<T5, T> f6) {
        return extractNullSafe(extract5(source, f1, f2, f3, f4, f5), f6);
    }

    /**
     * Extracts target object from source object using seven mapper functions.
     *
     * @param <S>    Source type
     * @param <T>    Target type
     * @param <T1>   Intermediate type
     * @param <T2>   Intermediate type
     * @param <T3>   Intermediate type
     * @param <T4>   Intermediate type
     * @param <T5>   Intermediate type
     * @param <T6>   Intermediate type
     * @param source Source object
     * @param f1     First mapper
     * @param f2     Second mapper
     * @param f3     Third mapper
     * @param f4     Fourth mapper
     * @param f5     Fifth mapper
     * @param f6     Sixth mapper
     * @param f7     Seventh mapper
     * @return Target object.
     */
    private static <S, T, T1, T2, T3, T4, T5, T6> T extract7(S source,
                                                             Function<S, T1> f1,
                                                             Function<T1, T2> f2,
                                                             Function<T2, T3> f3,
                                                             Function<T3, T4> f4,
                                                             Function<T4, T5> f5,
                                                             Function<T5, T6> f6,
                                                             Function<T6, T> f7) {
        return extractNullSafe(extract6(source, f1, f2, f3, f4, f5, f6), f7);
    }

}

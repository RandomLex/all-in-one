package safeextractor;

public class SafeExtractorDemo {
    public static void main(String[] args) {
        A a = new A(new B(new C("secret")));

        String c = SafeExtractor.extract(a, A::getB, B::getC, C::getSecret).orElse(null);
        System.out.println(c);
    }
}

class A {
    private final B b;

    public A(B b) {
        this.b = b;
    }

    public B getB() {
        return b;
    }
}

class B {
    private C c;

    public B(C c) {
        this.c = c;
    }

    public C getC() {
        return c;
    }
}

class C {
    private final String secret;

    public C(String secret) {
        this.secret = secret;
    }

    public String getSecret() {
        return secret;
    }

}
/**
 * @author alexej_barzykin@epam.com
 */
public class Substr {
    public static void main(String[] args) {
        String str = "2018-01-15";
        System.out.println(str.substring(0, 4));
        System.out.println(str.substring(5, 7));
    }
}

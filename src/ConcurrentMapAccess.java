import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author alexej_barzykin@epam.com
 */
public class ConcurrentMapAccess {
    public static void main(String[] args) {


        Map<Integer, String> firstCustomerConsumedMap = new HashMap<Integer, String>();
        firstCustomerConsumedMap.put(0, "000");
        firstCustomerConsumedMap.put(1, "111");
        firstCustomerConsumedMap.put(2, "222");
        firstCustomerConsumedMap.put(3, "333");
//        firstCustomerConsumedMap.put(4, "444");
//        firstCustomerConsumedMap.put(5, "555");
//        firstCustomerConsumedMap.put(6, "666");
//        firstCustomerConsumedMap.put(7, "777");
//        firstCustomerConsumedMap.put(8, "888");
//        firstCustomerConsumedMap.put(9, "999");
//        firstCustomerConsumedMap.put(10, "AAA");
//        firstCustomerConsumedMap.put(11, "BBB");
//        firstCustomerConsumedMap.put(12, "CCC");
//        firstCustomerConsumedMap.put(13, "DDD");

        for (Map.Entry<Integer, String> entry : firstCustomerConsumedMap.entrySet()) {
            System.out.println(entry.getKey() + " -> " + entry.getValue());
        }
        Map<Integer, String> copyMap = new HashMap<Integer, String>(firstCustomerConsumedMap);
        Iterator<Integer> copyMapIt = firstCustomerConsumedMap.keySet().iterator();

        int paymentScheduleToRemoveSequence = 2;


        for (Map.Entry<Integer, String> entry : firstCustomerConsumedMap.entrySet()) {
            if (entry.getKey() >= paymentScheduleToRemoveSequence) {
                entry.setValue(firstCustomerConsumedMap.get(entry.getKey() + 1));
            }
        }

//
//        while (copyMapIt.hasNext())
//        {
//            Integer key = copyMapIt.next();
//
//            if (key.intValue() == paymentScheduleToRemoveSequence)
//            {
//                firstCustomerConsumedMap.put(key, null);
//            }
//            else if (key.intValue() > paymentScheduleToRemoveSequence)
//            {
//                String fauxPaymentSchedule = firstCustomerConsumedMap.get(key);
//                firstCustomerConsumedMap.put(key, null);
//                firstCustomerConsumedMap.put(new Integer(key.intValue() - 1), fauxPaymentSchedule);
//            }
//        }


        for (Map.Entry<Integer, String> entry : firstCustomerConsumedMap.entrySet()) {
            System.out.println(entry.getKey() + " -> " + entry.getValue());
        }


    }
}

package insertsorting;

import java.util.Arrays;

/**
 * Демонстрация сортировки вставками
 * @author alexej_barzykin@epam.com
 */
public class InsertSortingDemo {
    /**
     * Демонстрационный метод
     * @param args аргументы программы
     */
    public static void main(String[] args) {
        int[] array = {1, 2, 2, 4, 7, 10, 12, 14, 14, 17, 20, 3, 5, 20, 18, 13};
        System.out.println(Arrays.toString(array));
        int lastSortedIndex = searchLastSortedIndex(array);
        insertSorting(array, lastSortedIndex);
        System.out.println(Arrays.toString(array));
    }

    /**
     * Поиск индееса последнего отсортированного элемента в массиве
     * @param array целочисленный массив
     * @return индекс последнего отсортированного элемента
     */
    private static int searchLastSortedIndex(int[] array) {
        if (array.length < 2) {
            return 0;
        }
        int i = 0;
        while (array.length > i + 1 && array[i + 1] >= array[i]) {
            i++;
        }
        return i;
    }

    /**
     * Сортировка неотсортированного массива вставками
     * @param array исходный массив
     */
    private static void insertSorting(int[] array) {
        insertSorting(array, 0);
    }

    /**
     * Сортировка массива вставками в предположение того,
     * что часть массива он 0 до lastSortedIndex уже отсортирована
     * @param array исходный массив
     * @param lastSortedIndex индекс последнего отсортированного элемента
     */
    private static void insertSorting(int[] array, int lastSortedIndex) {
        for (int i = lastSortedIndex; i < array.length; i++) {
            int position = binarySearch(array, lastSortedIndex++, array[array.length - 1]);
            if (position < 0) {
                continue;
            }
            rightShiftCycled(array, position, array.length - 1);
        }
    }

    /**
     * Бинарный поиск индекса в отсортированной части массива, куда нужно вставить checkingNumber
     * Отсортированная часть массива находится в границах от 0-го до lastSortedIndex
     * @param array исходный массив
     * @param lastSortedIndex индекс последнего отсортированного элемента
     * @param checkingNumber проверяемое число
     * @return индекс, куда нужно вставить проверяемое число
     */
    private static int binarySearch(int[] array, int lastSortedIndex, int checkingNumber) {
        int begin = -1;
        int end = lastSortedIndex;
        int mid;
        while (end > begin + 1) {
            mid = begin + (end - begin) / 2;
            if (checkingNumber < array[mid]) {
                end = mid;
            } else {
                begin = mid;
            }
        }
        return end;
    }

    /**
     * Циклический сдвиг части массива от элемента fromIndex по элемент toIndex вправо.
     * Элемент на месте toIndex будет вставлен на место fromIndex
     * @param array исходный массив
     * @param fromIndex индекс элемента, с которого начинается сдвиг
     * @param toIndex индекс элемента, которым заканчивается сдвиг
     */
    private static void rightShiftCycled(int[] array, int fromIndex, int toIndex) {
        int temp = array[toIndex];
        for (int i = toIndex - 1; i >= fromIndex; i--) {
            array[i+1] = array[i];
        }
        array[fromIndex] = temp;
    }
}

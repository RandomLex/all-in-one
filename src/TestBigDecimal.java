import java.math.BigDecimal;

/**
 * @author alexej_barzykin@epam.com
 */
public class TestBigDecimal {
    public static void main(String[] args) {
        BigDecimal one = BigDecimal.valueOf(25.12);
        System.out.println(one.toString());
    }
}

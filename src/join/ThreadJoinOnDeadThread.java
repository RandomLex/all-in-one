package join;

/**
 * @author alexej_barzykin@epam.com
 */
public class ThreadJoinOnDeadThread {
    public static void main(String[] args) throws InterruptedException {
        Thread thread1 = new Thread(() -> {
            try {
                Thread.sleep(10_000);
                System.out.println("thread 1 has finished");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread thread2 = new Thread(() -> {
            try {
                Thread.sleep(5_000);
                System.out.println("thread 2 has finished");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();

    }

}

package maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author alexej_barzykin@epam.com
 */
public class DemoOptions {
//    public static void main(String[] args) {
//        Engine engOne = new Engine();
//        engOne.setName("6 cylinders");
//        Engine engTwo = new Engine();
//        Engine engThree = new Engine();
//        engThree.setName("8 cylinders");
//
//        Auto ford = new Auto();
//        ford.setEngine(engOne);
//        Auto shkoda = new Auto();
//        shkoda.setEngine(engTwo);
//        Auto vw = new Auto();
//        vw.setEngine(engThree);
//
//        List<Auto> autos = new ArrayList<>();
//        autos.add(ford);
//        autos.add(shkoda);
//        autos.add(vw);
//
//        List<String> names = autos.stream()
////                .filter(auto -> auto.getEngine().getName() != null)
//                .map(auto -> auto.getEngine().getName())
//                .filter(Objects::nonNull)
//                .collect(Collectors.toList());
//        names.forEach(System.out::println);
//
//        Audio sony = new Audio("Sony");
//        Audio blaupunkt = new Audio("Blaupunkt");
//
//        ford.setAudio(sony);
//        vw.setAudio(blaupunkt);
//
//
//        List<String> audioNames = autos.stream()
//                .map(Auto::getAudio)
//                .flatMap(Optional::stream)
//                .map(Audio::getFirmName)
//                .collect(Collectors.toList());
//
//        audioNames.forEach(System.out::println);
//    }
//
//    private static class Auto {
//        private Engine engine;
//        private Audio audio;
//
//        public Engine getEngine() {
//            return engine;
//        }
//
//        public void setEngine(Engine engine) {
//            this.engine = engine;
//        }
//
//        public Optional<Audio> getAudio() {
//            return Optional.ofNullable(audio);
//        }
//
//        public void setAudio(Audio audio) {
//            this.audio = audio;
//        }
//    }
//
//    private static class Engine {
//        private String name;
//
//        public String getName() {
//            return name;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//    }
//
//    private static class Audio {
//        private String firmName;
//
//        public String getFirmName() {
//            return firmName;
//        }
//
//        public void setFirmName(String firmName) {
//            this.firmName = firmName;
//        }
//
//        public Audio(String firm) {
//            this.firmName = firm;
//        }
//    }

}

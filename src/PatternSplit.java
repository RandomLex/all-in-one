import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author alexej_barzykin@epam.com
 */
public class PatternSplit {
    public static void main(String[] args) {
        List<String> in = List.of(
                "name",
                "name-1",
                "name-123",
                "name-123-1",
                "name-123-456"
        );
        Pattern pa = Pattern.compile("-\\d+$");

        in.forEach(s -> {
            System.out.println(s + " -> " + Arrays.stream(pa.split(s)).findFirst().orElse(""));
            Matcher matcher = pa.matcher(s);
            while (matcher.find()) {
                System.out.println(matcher.group());
            }
        });
    }
}

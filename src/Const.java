/**
 * @author alexej_barzykin@epam.com
 */
public class Const {
    private static int my = 55000;

    public  int add(int i) {
        return i + my;
    }

    public  int sub(int i) {
        return i - my;
    }

    public static void main(String[] args) {
        Const c = new Const();
        System.out.println(c.add(3));
        System.out.println(c.sub(60000));

    }
}

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @author alexej_barzykin@epam.com
 */
public class DaysRound {
    private static final DateTimeFormatter YYYY_MM_DD_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static void main(String[] args) {

        String fromTime = "2017-01-04";

        String roundedFromTime = LocalDate.parse(fromTime, YYYY_MM_DD_FORMATTER)
                .with(DayOfWeek.SUNDAY).format(YYYY_MM_DD_FORMATTER);

        System.out.println(roundedFromTime);
    }
}

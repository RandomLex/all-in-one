package supressed;

import java.io.IOException;

/**
 * @author alexej_barzykin@epam.com
 */
public class OurResource implements AutoCloseable {

    public void perform() {
        throw new ArrayIndexOutOfBoundsException("perform exception");
    }

    @Override
    public void close() throws Exception {
        throw new IOException("close exception");
    }
}

package supressed;

/**
 * @author alexej_barzykin@epam.com
 */
public class OurClass {
    public static void main(String[] args) {
        try (OurResource ourResource = new OurResource()) {
            ourResource.perform();
            System.out.println("It won't be printed");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            for (Throwable throwable : e.getSuppressed()) {
                System.out.println(throwable.getMessage());
                System.out.println(throwable);
            }

        }
    }
}

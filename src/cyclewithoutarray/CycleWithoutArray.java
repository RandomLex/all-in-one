package cyclewithoutarray;

import java.util.Scanner;

public class CycleWithoutArray {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int iterations = sc.nextInt();
        String result = "";
        for (int i = 0; i < iterations; i++) {
            if (i != 0) {
                result += "\n";
             }
            result += sc.next();
        }
        System.out.println(result);
    }
}

package cyclewithoutarray;

import java.util.Scanner;

public class CycleWithoutArrayStringBuilder {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int iterations = sc.nextInt();
        sc.nextLine();

        if (iterations < 1) {
            return;
        }

        StringBuilder result = new StringBuilder();
        result.append(sc.next());
        for (int i = 1; i < iterations; i++) {
            result.append("\n");
            result.append(sc.next());
        }
        System.out.println(result);
    }
}
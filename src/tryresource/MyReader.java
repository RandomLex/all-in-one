package tryresource;

/**
 * @author alexej_barzykin@epam.com
 */
public class MyReader implements AutoCloseable {
    private String resourse = "SuperData";



    public String getResourse() throws MyException {
        return resourse;
    }

    @Override
    public void close() throws MyException {
        System.out.println("Resource has closed");
        Integer[] integ = new Integer[3];
        System.out.println("integ.getClass().getSimpleName() = " + integ.getClass().getName());
    }
}

package tryresource;

/**
 * @author alexej_barzykin@epam.com
 */
public class Runner {
    public static void main(String[] args) {

        try (MyReader reader = new MyReader()) {
            System.out.println(reader.getResourse());
        } catch (MyException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("Finish");
    }
}

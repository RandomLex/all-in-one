package tryresource;

/**
 * @author alexej_barzykin@epam.com
 */
public class StaticCall {
    public static void main(String[] args) {
        ClassA classA = null;
        classA.printHello();
    }
}

/**
 * @author alexej_barzykin@epam.com
 */
public class StringReplaceQuote {
    public static void main(String[] args) {
        String a = "\"abc\" : \"123\"";

        String b = a.replaceAll("\"", "\\\\\"");

        System.out.println(b);
    }
}

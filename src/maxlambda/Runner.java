package maxlambda;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * @author alexej_barzykin@epam.com
 */
public class Runner {
    public static void main(String[] args) {
        List<Horse> horses = new ArrayList<>();
        horses.add(new Horse(7));
        horses.add(new Horse(10.));
        horses.add(new Horse(3));

        horses.forEach(System.out::println);

        horses.stream()
                .max((h1, h2) -> (int) (h1.getDistance() - h2.getDistance()))
                .ifPresent(System.out::println);

        horses.stream().max(Comparator.comparing(Horse::getDistance))
                .ifPresent(System.out::println);


    }
}

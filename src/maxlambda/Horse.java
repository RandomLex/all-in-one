package maxlambda;

/**
 * @author alexej_barzykin@epam.com
 */
public class Horse {
    private double distance;

    public Horse(double distance) {
        this.distance = distance;
    }

    public double getDistance() {
        return distance;
    }

    @Override
    public String toString() {
        return "Horse{" +
                "distance=" + distance +
                '}';
    }
}

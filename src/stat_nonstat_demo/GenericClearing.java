package stat_nonstat_demo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author alexej_barzykin@epam.com
 */
public class GenericClearing {
  public static void main(String[] args) {
    A a = new A();
    B b = new B();

    List<A> list = new ArrayList<>();
    list.add(a);
    list.add(b);

    for (A x : list) {
      x.d0Smth();
    }

  }

  private static class A {
    public void d0Smth() {
      System.out.println("A");
    }
  }

  private static class B extends A {
    @Override
    public void d0Smth() {
      System.out.println("B");
    }
  }
}

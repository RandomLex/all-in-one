package stat_nonstat_demo;

/**
 * @author alexej_barzykin@epam.com
 */
public class Person {
  private static String prefix;
  private String name;
  private int age;

  public Person() {
  }

  public Person(String name, int age) {
    this.name = name;
    this.age = age;
  }

  public static String getPrefix() {
    return prefix;
  }

  public static void setPrefix(String prefix) {
    Person.prefix = prefix;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public String toString() {
    return prefix + " " + name + " " + age;
  }

  public static String bingo() {
    return "Bingo";
  }

  public int sum(int a, int b) {
    return a + b;
  }

  public boolean isAdult() {
    if (age >= 18) {
      return true;
    } else {
      return false;
    }
  }

  public boolean isAdultForCountry(int borderAge) {
    if (age >= borderAge) {
      return true;
    } else {
      return false;
    }
  }


}

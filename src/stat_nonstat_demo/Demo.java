package stat_nonstat_demo;

/**
 * @author alexej_barzykin@epam.com
 */
public class Demo {
  static int staticA;
  int a;
  public static void main(String[] args) {
    Demo demo = new Demo();
    demo.a = 5;
    Demo demo2 = new Demo();
    demo2.a =6;
    System.out.println(demo.a);
    staticA = 5;
    System.out.println(staticA);
  }
}

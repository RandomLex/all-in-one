package stat_nonstat_demo;

/**
 * @author alexej_barzykin@epam.com
 */
public class Start {
  public static void main(String[] args) {
    Person.setPrefix("Mr.");
    Person person1 = new Person("Иван", 20);
    Person personIlia = new Person("Илья", 43);
    person1.setAge(21);

    Person person2;
    person2 = person1;
    person2.setAge(35);


    person1.bingo();
    person2.bingo();
    personIlia.bingo();

    person1.getAge();
    person2.getAge();
    personIlia.getAge();

    System.out.println(person1.sum(3, 5));


    System.out.println(person1.getAge());

    System.out.println(person1.isAdultForCountry(21));
  }
}

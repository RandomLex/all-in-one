import java.util.HashSet;
import java.util.Set;

/**
 * @author alexej_barzykin@epam.com
 */
public class EqualsConst {
    public static void main(String[] args) {
        Set<String> setOne = new HashSet<>();
        Set<String> setTwo = new HashSet<>();

        setOne.add("One");
        setOne.add("Two");
        setOne.add("Three");

        setTwo.add("Three");
        setTwo.add("Fife");
        setTwo.add("Six");

        setOne.retainAll(setTwo);


        setOne.forEach(System.out::println);
        setOne.forEach(System.out::println);

    }
}

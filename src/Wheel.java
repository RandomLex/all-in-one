import java.io.Serializable;

/**
 * @author alexej_barzykin@epam.com
 */
public class Wheel implements Serializable{
    private String brand;
    private int radius;

    public Wheel() {
    }

    public Wheel(String brand, int radius) {
        this.brand = brand;
        this.radius = radius;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public Wheel withRadius(int radius) {
        this.setRadius(radius);
        return this;
    }

    public Wheel withBrand(String brand) {
        this.setBrand(brand);
        return this;
    }

    @Override
    public String toString() {
        return "Wheel{" +
                "brand='" + brand + '\'' +
                ", radius=" + radius +
                '}';
    }
}

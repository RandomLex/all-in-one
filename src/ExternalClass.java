/**
 * @author alexej_barzykin@epam.com
 */
public class ExternalClass {
  static void handle(StaticNonStatic s) {
    StaticNonStatic.staticOne = 4;
    StaticNonStatic ourExempliar = new StaticNonStatic();
    ourExempliar.nonStaticOne = 12;
    s.nonStaticOne = 14;
  }
}

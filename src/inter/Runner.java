package inter;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This is an example of usage the simple wrappers for ThirdParty classes
 * with the same functionality and the same method's signatures.
 */
public class Runner {

    public static void main(String[] args) {
        List<Writable> writables = Stream.of(
                new MyFirstWriter(),
                new MySecondWriter(),
                new ThirdPartyWriterTheFirst(),
                new ThirdPartyWriterTheSecond()
        ).collect(Collectors.toList());

        writables
                .forEach(Writable::write);

    }

    //We have an hierarchy from Writable interface
    interface Writable {
        void write();
    }

    static class MyFirstWriter implements Writable {

        @Override
        public void write() {
            System.out.println("MyFirstWriter has written");
        }
    }

    static class MySecondWriter implements Writable {

        @Override
        public void write() {
            System.out.println("MySecondWriter has written");
        }
    }

    //Pay your attention that this Writer is not from Writable hierarchy
    static class ProprietaryPartyWriterTheFirst {
        public void write() {
            System.out.println("ProprietaryPartyWriterTheFirst has written");
        }
    }

    //Pay your attention that this Writer is not from Writable hierarchy too
    static class ProprietaryPartyWriterTheSecond {
        public void write() {
            System.out.println("ProprietaryPartyWriterTheSecond has written");
        }
    }

    //We can use just simple wrapper here. Without any getters-setters logic
    private static class ThirdPartyWriterTheFirst extends ProprietaryPartyWriterTheFirst implements Writable {}

    private static class ThirdPartyWriterTheSecond extends ProprietaryPartyWriterTheSecond implements Writable {}


}

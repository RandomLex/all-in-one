package recursion;

public class SumRecursion {
    public static void main(String[] args) {
        int sum = sum(5);
        System.out.println(sum);
    }

    private static int sum(int i) {
        if (i == 1) {
            return 1;
        }
        return i + sum(i - 1);
    }
}

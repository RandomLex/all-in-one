package bigdec;

import java.math.BigDecimal;

/**
 * @author alexej_barzykin@epam.com
 */
public class StarterCompare {
    public static void main(String[] args) {
        BigDecimal positive = BigDecimal.valueOf(3.15);
        BigDecimal negative = new BigDecimal("-3.15");

        System.out.println(positive.compareTo(BigDecimal.ZERO));
        System.out.println(negative.compareTo(BigDecimal.ZERO));
    }
}

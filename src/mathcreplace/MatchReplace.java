package mathcreplace;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author alexej_barzykin@epam.com
 */
public class MatchReplace {
    public static void main(String[] args) {
        String in = "1231234abcdeffgh";
        Pattern p = Pattern.compile("b|ff|d");
        Matcher m = p.matcher(in);
        List<String> outList = new ArrayList<>();
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(sb, up(m.group()));
        }
//        m.appendTail(sb);
        System.out.println(sb.toString());

    }

    private static String up(String str) {
        return str.toUpperCase();
    }
}

import java.io.Serializable;

/**
 * @author alexej_barzykin@epam.com
 */
public class Engine implements Serializable {
    private String brand;
    private int power;

    public Engine() {
    }

    public Engine(String brand, int power) {
        this.brand = brand;
        this.power = power;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public Engine withBrand(String brand) {
        this.brand = brand;
        return this;
    }

    public Engine withPower(int power) {
        this.power = power;
        return this;
    }

    @Override
    public String toString() {
        return "Engine{" +
                "brand='" + brand + '\'' +
                ", power=" + power +
                '}';
    }
}

/**
 * @author alexej_barzykin@epam.com
 */
public class SplitDemo {
    public static void main(String[] args) {
        String str = "12345";
        String[] split = str.split("");
        for (String s : split) {
            System.out.println(": " + s);
        }


    }
}

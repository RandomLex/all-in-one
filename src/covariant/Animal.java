package covariant;

/**
 * @author alexej_barzykin@epam.com
 */
public class Animal {
    protected String name;

    public Animal() {
    }

    public Animal(String name) {
        this.name = name;
    }

    public void move() {
        System.out.println(name + " is moving");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

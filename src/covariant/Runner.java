package covariant;

import java.util.ArrayList;
import java.util.List;

/**
 * @author alexej_barzykin@epam.com
 */
public class Runner {
    public static void main(String[] args) {
        List<Animal> animals = new ArrayList<>();
        animals.add(new Animal("Неведома козявка"));
        animals.add(new Animal("Неведома рыбка"));
        animals.add(new Cat("Некий котик"));
        animals.add(new Bird("Синяя птичка"));

        List<Cat> cats = new ArrayList<>();
        cats.add(new Cat("Васька"));
        cats.add(new Cat("Барсик"));


        runAnimal(animals);
        System.out.println();
        runAnimal(cats);

        Animal[] animalsArray = new Animal[3];
        animalsArray[0] = new Animal("Букашка");
        animalsArray[1] = new Cat("Петька");
        animalsArray[2] = new Bird("Чижик");

        Cat[] catsArray = new Cat[2];
        catsArray[0] = new Cat("Пушок");
        catsArray[1] = new Cat("Марсик");

        System.out.println("\nЖивотные");
        runAnimal(animalsArray);
        System.out.println("\nКоты");
        runAnimal(catsArray);
        animalsArray = catsArray;
        System.out.println("\nКоты как животные");
        runAnimal(animalsArray);
        runVarargs(new Cat("Вася"), new Bird("Зюзя"));

    }

    public static void runAnimal(List<? extends Animal> animals) {
        for (Animal animal : animals) {
            animal.move();
        }

    }

    public static void runAnimal(Animal[] animals) {
        for (Animal animal : animals) {
            animal.move();
        }
    }


    @SafeVarargs
    public static <T extends Animal> void runVarargs(T ... elements) {
        for (T element : elements) {
            element.move();
        }

    }

}

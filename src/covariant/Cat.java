package covariant;

/**
 * @author alexej_barzykin@epam.com
 */
public class Cat extends Animal {

    public Cat() {
    }

    public Cat(String name) {
        super(name);
    }

    @Override
    public void move() {
        System.out.println(name + " is running");
    }
}

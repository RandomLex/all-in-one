package covariant;

/**
 * @author alexej_barzykin@epam.com
 */
public class Bird extends Animal {

    public Bird() {
    }

    public Bird(String name) {
        super(name);
    }

    @Override
    public void move() {
        System.out.println(name + " is flying");
    }
}

import java.util.ArrayList;
import java.util.List;

/**
 * @author alexej_barzykin@epam.com
 */
public class ListOfLists {
    public static void main(String[] args) {
        List<List<String>> parentList = new ArrayList<>();

        List<String> childZero = new ArrayList<>();
        List<String> childOne = new ArrayList<>();
        List<String> childTwo = new ArrayList<>();

        childZero.add("0-0");
        childOne.add("0-1");
        childTwo.add("0-2");
        childOne = List.of("1-0", "1-1", "1-2");
        childTwo = List.of("2-0", "2-1", "2-2");

        parentList.add(childZero);
        parentList.add(childOne);
        parentList.add(childTwo);

        System.out.println(parentList.get(0).get(1).toUpperCase().replace("-", "!"));

    }
}

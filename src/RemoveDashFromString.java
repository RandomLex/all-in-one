/**
 * @author alexej_barzykin@epam.com
 */
public class RemoveDashFromString {

    public static void main(String[] args) {
        String route = "ABC-DEF";
        System.out.println(removeDashFromRoute(route));
    }

    public static String removeDashFromRoute(String route) {
        if (route == null || "".equals(route)) {
            return "";
        } else {
            return route.substring(0, route.indexOf('-')).trim() + route.substring(route.indexOf('-') + 1).trim();
        }
    }
}

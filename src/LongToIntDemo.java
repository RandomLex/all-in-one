/**
 * @author alexej_barzykin@epam.com
 */
public class LongToIntDemo {
    public static void main(String[] args) {
        long x = 283479L;

        int y = toInt(x);

        System.out.println(y);
        System.out.println(toIntViaLong(x));
    }

    private static int toInt(long x) {
        return (int) x;
    }

    private static int toIntViaLong(long x) {
        return Long.valueOf(x).intValue();

    }
}

package tasks;

public class TeamScores {
    public static void main(String[] args) {
//        int[][][] m = new int[3][4][3];
        int[][][] m = {
                {{1, 0, 10}, {2, 15, 30}, {17, 8, 24}, {21, 39, 60}},
                {{3, 7, 57}, {51, 36, 15}, {15, 14, 17}, {12, 9, 42}},
                {{60, 40, 28}, {14, 24, 46}, {12, 7, 60}, {34, 23, 15}}
        };

        int[][] points = new int[m.length][m[0].length];
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
                for (int k = 0; k < m[i][j].length; k++) {
                    points[i][j] = points[i][j] + m[i][j][k];
                }
            }
        }

        int[] maxScores = new int[points.length];
        int[] players = new int[points.length];

        for (int i = 0; i < points.length; i++) {
            maxScores[i] = max(points[i]);
            for (int j = 0; j < points[i].length; j++) {
                if (maxScores[i] == points[i][j]) {
                    players[i] = j;
                }
            }
        }

        for (int i = 0; i < players.length; i++) {
            System.out.println("Winner of team " + (i + 1) + " is player " + (players[i] + 1) + " with " + maxScores[i] + " scores.");
        }

        System.out.println();
        int maxScoreOfTheCompetition = max(maxScores);
        for (int i = 0; i < maxScores.length; i++) {
            if (maxScoreOfTheCompetition == maxScores[i]) {
                System.out.println("Winner is the team " + (i + 1) + " is player " + (players[i] + 1) + " with " + maxScoreOfTheCompetition);
            }
        }
    }

    private static int max(int[] arr) {
        int max = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        return max;
    }
}
